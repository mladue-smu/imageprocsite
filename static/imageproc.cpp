#include <emscripten/bind.h>
#include <iostream>
#include <vector>

using namespace std;
using namespace emscripten;

// used for shared output (can consist of R, G, or B pixels)
vector<vector<unsigned char>> newPixels;

/* Calculates the convolution for a single pixel (x, y), given a kernel and a set of pixels of width w and
   height h. */
int convolution(vector<vector<double>>& kernel, int x, int y, int w, int h, vector<vector<unsigned char>>& pixels) {
        int sum = 0;
        for (int dx = -1; dx <= 1; dx++) {
            int i = dx;
            if (dx < 0) {
                i = dx + 3;
            }
            int fx = (x + dx) % h;
            if (fx < 0) {
                fx += h;
            }
            for (int dy = -1; dy <= 1; dy++) {
                int j = dy;
                if (dy < 0) {
                    j = dy + 3;
                }
                int fy = (y + dy) % w;
                if (fy < 0) {
                    fy += w;
                }
                sum += kernel[i][j] * pixels[fx][fy];
            }
        }
        if (sum < 0) {
            return 0;
        }
        if (sum > 255) {
            return 255;
        }
        return sum;
    }

/* Computes the convolution for part of an image. Corresponds to a single client. */
void convolve(int k, int t, int w, int h, vector<vector<double>> kernel, vector<vector<unsigned char>>& pixels) {
    int size = h / t;
    int max = size * (k + 1);
    if (k == t - 1) {
        // final thread should get all remaining indices
        max = h;
    }
    for (int i = size * k; i < max; i++) {
        for (int j = 0; j < w; j++) {
            newPixels[i][j] = convolution(kernel, i, j, w, h, pixels);
        }
    }
}

/**
 * The main class for our image processing library, which is transpiled into JS code using Embind.
 */
class ImageProc {

    private:
    vector<vector<unsigned char>> pixelsR;
    vector<vector<unsigned char>> pixelsG;
    vector<vector<unsigned char>> pixelsB;

    int t;
    int w;
    int h;

    public:
    /**
     * Class constructor: Takes in the image width, image height, and number of threads (set to 5 in the demo).
    */
    ImageProc(int width, int height, int numThreads) {
        w = width;
        h = height;
        t = numThreads;
        pixelsR = createVector(w, h);
        pixelsG = createVector(w, h);
        pixelsB = createVector(w, h);
    }

    /**
     * Sets the pixel at (x, y) to the RGB color given by (r, g, b).
    */
    void setPixel(int x, int y, int r, int g, int b) {
        pixelsR[x][y] = r;
        pixelsG[x][y] = g;
        pixelsB[x][y] = b;
    }
    private:
    /**
     * Initializes an empty 2D vector of the appropriate size that will hold pixel data.
    */
    static std::vector<vector<unsigned char>> createVector(int width, int height) {
        std::vector<vector<unsigned char>> pixels(height, vector<unsigned char>(width));
        return pixels;
    }
    /**
     * Generates the appropriate kernel corresponding to the given filter type.
    */
    vector<vector<double>> getKernel(int filterType) {
        // filterType indicates one of the following effects
        // 1: Mean Blur
        // 2: Gaussian Blur
        // 3: Sharpen
        // 4: Emboss (horizontal Sobel filter)
        // 5: Emboss (vertical Sobel filter)
        // 6: Laplacian
        vector<vector<double>> kernel1(3, vector<double>(3, 1. / 9));
        vector<vector<double>> kernel2 {vector<double> {1./16, 2./16, 1./16}, vector<double>{2./16, 4./16, 2./16}, vector<double>{1./16, 2./16, 1./16}};
        vector<vector<double>> kernel3 {vector<double> {0, -1, 0}, vector<double>{-1, 5, -1}, vector<double>{0, -1, 0}};
        vector<vector<double>> kernel4 {vector<double> {-1, -2, -1}, vector<double>{0, 0, 0}, vector<double>{1, 2, 1}};
        vector<vector<double>> kernel5 {vector<double> {-1, 0, 1}, vector<double>{-2, 0, 2}, vector<double>{-1, 0, 1}};
        vector<vector<double>> kernel6 {vector<double> {0, 1, 0}, vector<double>{1, -4, 1}, vector<double>{0, 1, 0}};
        switch (filterType) {
            case 1:
            return kernel1;
            case 2:
            return kernel2;
            case 3:
            return kernel3;
            case 4:
            return kernel4;
            case 5:
            return kernel5;
            case 6:
            return kernel6;
            default:
            return kernel1; // should never happen
        }
    }
    public:
    /**
     * Processes the red pixels of the image according to the given filterType.
    */
    vector<vector<unsigned char>> processR(int filterType) {
        vector<vector<double>> kernel = getKernel(filterType);
        newPixels = createVector(w, h);
        for(int i = 0; i < t; i++) {
            convolve(i, t, w, h, kernel, pixelsR);
        }
        return newPixels;
    }
    /**
     * Processes the green pixels of the image according to the given filterType.
    */
    vector<vector<unsigned char>> processG(int filterType) {
        vector<vector<double>> kernel = getKernel(filterType);
        newPixels = createVector(w, h);
        for(int i = 0; i < t; i++) {
            convolve(i, t, w, h, kernel, pixelsG);
        }
        return newPixels;
    }
    /**
     * Processes the blue pixels of the image according to the given filterType.
    */
    vector<vector<unsigned char>> processB(int filterType) {
        vector<vector<double>> kernel = getKernel(filterType);
        newPixels = createVector(w, h);
        for(int i = 0; i < t; i++) {
            convolve(i, t, w, h, kernel, pixelsB);
        }
        return newPixels;
    }
};
// Emscripten bindings for transpiling using Embind
EMSCRIPTEN_BINDINGS(my_class_example) {
  class_<ImageProc>("ImageProc")
    .constructor<int, int, int>()
    .function("processR", &ImageProc::processR)
    .function("processG", &ImageProc::processG)
    .function("processB", &ImageProc::processB)
    .function("setPixel", &ImageProc::setPixel);

    register_vector<unsigned char>("vector<unsigned char>");
    register_vector<vector<unsigned char>>("vector<vector<unsigned char>>");
}
