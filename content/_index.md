## Image Processor

A simple image processor with 6 different filters.

Click on the link in the top bar to try out a live web demo, or download a native app for MacOS.
